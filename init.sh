#!/bin/zsh

if [ -f "${HOME}/setup/mac/init.sh" ]; then
  echo "setup file found"
  cd ~/setup/mac
  source ./init.sh
  return 0
fi

cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P

GITLAB_USER="niels.nielsen"
EMAIL="${GITLAB_USER}@gmail.com"

export XDG_CONFIG_HOME="${HOME}/.config"
mkdir -p $XDG_CONFIG_HOME/lpass

echo "User password:"
IFS= read -rs USER_PW
sudo -k
if sudo -v -S &> /dev/null << EOF
$USER_PW
EOF
then
  echo "\nPassword confirmed."
else
  echo "\nInvalid password."
  sudo -k
  exit 1
fi
sudo -k

echo "Lastpass password:"
IFS= read -rs LASTPASS_PW

echo '/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"' > homebrew_install0.sh
chmod +x homebrew_install0.sh

expect <<EOF
set timeout -1
spawn ./homebrew_install0.sh

expect "Password:"
send -- "$USER_PW\r"

expect -re {to continue or any other key to abort:\r\n$}
send -- "\r"

expect eof
EOF

eval "$(/opt/homebrew/bin/brew shellenv)"
echo "Homebrew installed, run brew doctor to verify"

brew install lastpass-cli

LPASS_DISABLE_PINENTRY=1 expect << EOF
set timeout -1
spawn lpass login $EMAIL

expect -re {Master Password.* $}
send -- "$LASTPASS_PW\r"

expect eof
EOF

GITLAB_PW=$(lpass show gitlab --password)
lpass logout -f

URLENCODED_PW=$(perl -MURI::Escape -e 'print uri_escape($ARGV[0])', "$GITLAB_PW")
git clone "https://${GITLAB_USER}:${URLENCODED_PW}@gitlab.com/${GITLAB_USER}/setup" ~/setup

cd ~/setup/mac

if [ -f "init.sh" ]; then
  source ./init.sh
else
  echo "Could not run init script, something failed and setup repo is not checked out"
fi
